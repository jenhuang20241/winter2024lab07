public class Card
{
	private String suit;
	private String value;
	
	
	//constructor
	public Card (String suit, String value)
	{
		this.suit = suit;
		this.value = value;
	}

	//get methods
	public String getSuit()
	{
		return this.suit;
	}
	public String getValue()
	{
		return this.value;
	}
	
	//toString method
	public String toString()
	{
		return this.value + " of " + this.suit;
	}
	
	//custom method to calculate the worth of each card
	public double calculateScore()
	{
		double score = Double.parseDouble(this.value);
		
		if (this.suit.equals("\u001B[31m"+"Hearts"+"\u001B[0m")) {
			
			score += 0.4;
		
		} else if (this.suit.equals("\u001B[31m"+"Diamonds"+"\u001B[0m")) {
			
			score += 0.3;
		
		} else if (this.suit.equals("Clubs")) {
			
			score += 0.2;
		
		} else {
			
			score += 0.1;
		}
		return score;
	}
}